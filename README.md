This Singularity image is intended to serve as base for all project images. 

By default it starts up RStudio with an auto-selected port and password


# Pulling the Singularity Image

```
export IMAGE_TAG="R4.1.2"
export IMAGE_DIR="/work/${USER}/images"
mkdir -p ${IMAGE_DIR}
singularity pull --force --dir ${IMAGE_DIR} oras://gitlab-registry.oit.duke.edu/chsi-informatics/containers/singularity-rstudio-base:${IMAGE_TAG}
```

# Managing tags
## Adding Tags
```
git tag -a R4.1.2 -m "R4.1.2, ubuntu22"
git push origin --tags
```
## Deleting Tags
```
git tag -d R4.1.2
git push origin --delete  R4.1.2
```

## /tmp issues
It is recommended to do one of the following when running this image. There is no need to do both:

1. Set "mount tmp = no" in `/etc/singularity/singularity.conf`.
2. If #1 is not an option, the following command can be used to bind mount `/tmp` in the container to a "private" tmp directory:
```
SINGTMP="/tmp/${USER}_$$_tmp"; mkdir -p $SINGTMP; singularity run --bind $SINGTMP:/tmp shub://granek/singularity-rstudio-base
```
### /tmp issues TLDR
If a second user tries on the same server tries to run an RStudio container they will have permission issues with `/tmp/rstudio-server`, which will be owned by the user who first ran an RStudio container.
